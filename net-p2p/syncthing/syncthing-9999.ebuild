# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit eutils base git-r3 systemd

DESCRIPTION="Open, trustworthy and decentralized syncing engine"
HOMEPAGE="http://${PN}.net"

EGIT_REPO_URI="https://github.com/${PN}/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="systemd"

DEPEND="
	>=dev-lang/go-1.3
	app-misc/godep
"
RDEPEND="${DEPEND}"

DOCS=( README.md AUTHORS LICENSE CONTRIBUTING.md )

export GOPATH="${S}"

GO_PN="github.com/${PN}/${PN}"
EGIT_CHECKOUT_DIR="${S}/src/${GO_PN}"
S=${EGIT_CHECKOUT_DIR}

src_compile() {
	# XXX: All the stuff below needs for "-version" command to show actual info
	local version="$(git describe --always | sed 's/\([v\.0-9]*\)\(-\(beta\|alpha\)[0-9]*\)\?-/\1\2+/')";
	local date="$(git show -s --format=%ct)";
	local user="$(whoami)"
	local host="$(hostname)"; host="${host%%.*}";
	local lf="-w -X main.Version ${version} -X main.BuildStamp ${date} -X main.BuildUser ${user} -X main.BuildHost ${host}"

	godep go build -ldflags "${lf}" -tags noupgrade ./cmd/syncthing || die
}

src_install() {
	dobin syncthing
	base_src_install_docs
	if use systemd; then
		systemd_newunit "${FILESDIR}"/${PN}.service ${PN}@.service
	else
		newinitd ${FILESDIR}/rc/init ${PN}
		newconfd ${FILESDIR}/rc/conf ${PN}
	fi
}

pkg_postinst() {
	if use systemd; then
		ECHO_1="systemctl start ${PN}@<user>"
		ECHO_2="systemctl enable ${PN}@<user>"
	else
		ECHO_1="/etc/init.d/${PN} start"
		ECHO_2="rc-update add ${PN} default"
	fi

	einfo
	elog "To run syncthing as a service:"
	elog "  ${ECHO_1}"
	elog "To enable it at startup:"
	elog "  ${ECHO_2}"
	einfo
}