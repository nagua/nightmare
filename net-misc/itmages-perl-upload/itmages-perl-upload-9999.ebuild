# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit eutils git-r3

DESCRIPTION="Itmages console image uploader"
HOMEPAGE="http://itmages.ru"

EGIT_REPO_URI="https://github.com/itmages/itmages-perl-upload.git"

LICENSE="freedist"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="
	dev-lang/perl
	dev-perl/LWP-Protocol-https
	dev-perl/Net-HTTP
	dev-perl/Getopt-Long-Descriptive
	dev-perl/JSON
	dev-perl/Params-Validate
	dev-perl/Config-General
"
DEPEND="${RDEPEND}"

src_install() {
	exeinto /usr/share/itmages
	doexe upload.pl
	dosym /usr/share/itmages/upload.pl /usr/bin/itmages-upload
	einfo "Don't forget to run itmages-upload for the first"
	einfo "time to create an empty config."
}
