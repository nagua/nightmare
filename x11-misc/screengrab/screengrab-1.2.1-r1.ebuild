# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit cmake-utils versionator

DESCRIPTION="Qt application for getting screenshots"
HOMEPAGE="http://screengrab.doomer.org"

MYPV="$(replace_all_version_separators '_')"
SRC_URI="http://${PN}.doomer.org/download/${PN}-${MYPV}_/  -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	x11-libs/libX11
	dev-qt/qtcore:4
	dev-qt/qtgui:4
"

RDEPEND="${DEPEND}"

src_prepare() {
	# Install docs into the right dir, but skip the license.
	sed -i -e "/SG_DOCDIR/s:screengrab:${PF}:" \
		CMakeLists.txt || die
	cmake-utils_src_prepare
}
