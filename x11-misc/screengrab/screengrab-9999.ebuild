# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit cmake-utils git-r3
CMAKE_MIN_VERSION="2.8.11"

DESCRIPTION="Qt application for getting screenshots"
HOMEPAGE="http://screengrab.doomer.org"

#EGIT_REPO_URI="git://github.com/DOOMer/${PN}.git"
EGIT_REPO_URI="git://github.com/QtDesktop/${PN}.git"
EGIT_BRANCH="master"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	x11-libs/libX11
	>=dev-qt/qtcore-5.2:5
	>=dev-qt/qtgui-5.2:5[dbus]
	>=dev-qt/qtwidgets-5.2:5
	>=dev-qt/qtnetwork-5.2:5
	>=dev-qt/qtx11extras-5.2:5
	kde-frameworks/kwindowsystem:5
	dev-qt/linguist-tools:5
	dev-libs/libqtxdg
"

RDEPEND="${DEPEND}"

src_prepare() {
	cmake-utils_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DCMAKE_INSTALL_PREFIX="/usr"
		-DSG_XDG_CONFIG_SUPPORT=ON
		-DSG_EXT_UPLOADS=ON
		-DSG_DBUS_NOTIFY=ON
		-DSG_GLOBALSHORTCUTS=OFF
		-DSG_USE_SYSTEM_QXT=ON
		-DSG_DOCDIR="${PF}"
		-DQKSW_SHARED=ON
	)

	cmake-utils_src_configure
}
