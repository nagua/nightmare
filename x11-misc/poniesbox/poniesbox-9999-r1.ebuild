# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit qt4-r2 git-r3

LANGS="de el fr pl ru uk"

DESCRIPTION="Desktop ponies in Qt"
HOMEPAGE="https://bitbucket.org/XRevan86/${PN}"

EGIT_REPO_URI="https://bitbucket.org/XRevan86/${PN}.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-qt/qtcore:4
	dev-qt/qtgui:4
"

RDEPEND="${DEPEND}"
