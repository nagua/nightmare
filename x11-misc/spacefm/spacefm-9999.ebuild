# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit fdo-mime gnome2-utils linux-info git-r3

DESCRIPTION="A multi-panel tabbed file manager"
HOMEPAGE="http://ignorantguru.github.com/${PN}"

LICENSE="GPL-3+ LGPL-2.1+"
SLOT="0"
KEYWORDS=""
IUSE="+branch_master branch_next branch_alpha startup-notification -gtk3 +inotify -pixmaps -hal +dbus video-thumbnails desktop-integration"
REQUIRED_USE="^^ ( branch_master branch_next branch_alpha )"

# https://gitlab.com/r3lgar/nightmare/issues/2
IUSE_EFFECTIVE="branch_master branch_next branch_alpha"

EGIT_REPO_URI="git://github.com/IgnorantGuru/${PN}.git"

if use branch_master; then
	MY_EGIT_BRANCH="master"
elif use branch_next; then
	MY_EGIT_BRANCH="next"
elif use branch_alpha; then
	MY_EGIT_BRANCH="alpha"
fi

EGIT_BRANCH="${MY_EGIT_BRANCH}"

RDEPEND="
	dev-libs/glib:2
	dev-util/desktop-file-utils
	>=virtual/udev-143
	virtual/freedesktop-icon-theme
	x11-libs/cairo
	x11-libs/gdk-pixbuf[jpeg]
	gtk3? ( x11-libs/gtk+:3 )
	!gtk3? ( x11-libs/gtk+:2 )
	x11-libs/pango
	x11-libs/libX11
	x11-misc/shared-mime-info
	startup-notification? ( x11-libs/startup-notification )
"

DEPEND="
	${RDEPEND}
	dev-util/intltool
	virtual/pkgconfig
	sys-devel/gettext
	sys-process/lsof
	dbus? ( sys-apps/dbus )
	video-thumbnails? ( media-video/ffmpegthumbnailer )
"

src_configure() {
	econf \
		--htmldir=/usr/share/doc/${PF}/html \
		--with-bash-path=/bin/bash \
		$(use_enable startup-notification) \
		$(use_enable hal) \
		$(use_enable inotify) \
		$(use_enable pixmaps) \
		$(use_enable video-thumbnails) \
		$(use_with gtk3) \
		$(use_enable desktop-integration)
}

pkg_preinst() {
	gnome2_icon_savelist
}

pkg_postinst() {
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
	gnome2_icon_cache_update

	einfo
	elog "To mount as non-root user you need one of the following:"
	elog "  sys-apps/udevil (recommended, see below)"
	elog "  sys-apps/pmount"
	elog "  sys-fs/udisks:0"
	elog "  sys-fs/udisks:2"
	elog "To support ftp/nfs/smb/ssh URLs in the path bar you need:"
	elog "  sys-apps/udevil"
	elog "To perform as root functionality you need one of the following:"
	elog "  x11-misc/gtksu"
	elog "  x11-misc/ktsuss"
	elog "  x11-libs/gksu"
	elog "  kde-base/kdesu"
	elog "Other optional dependencies:"
	elog "  sys-apps/dbus"
	elog "  sys-process/lsof (device processes)"
	elog "  virtual/eject (eject media)"
	einfo
	if ! has_version 'sys-fs/udisks' ; then
		elog "When using SpaceFM without udisks, and without the udisks-daemon running,"
		elog "you may need to enable kernel polling for device media changes to be detected."
		elog "See /usr/share/doc/${PF}/html/spacefm-manual-en.html#devices-kernpoll"
		has_version '<virtual/udev-173' && ewarn "You need at least udev-173"
		kernel_is lt 2 6 38 && ewarn "You need at least kernel 2.6.38"
		einfo
	fi
	if ( ! has_version 'sys-apps/paludis' ); then
		elog
		ewarn "If you have warnings with IUSE_EFFECTIVE, sure to inform me:"
		ewarn "  https://gitlab.com/r3lgar/nightmare/issues/2"
	fi
}

pkg_postrm() {
	fdo-mime_desktop_database_update
	fdo-mime_mime_database_update
	gnome2_icon_cache_update
}
