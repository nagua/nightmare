# NIGHTMARE

**nightmare** is a [Gentoo Linux](https://gentoo.org) overlay with some stuff.

All ebuilds are tested and fully functional on **x86** and **amd64**.

* * *

## app-portage/layman

To add our overlay via [layman](https://wiki.gentoo.org/wiki/Layman) just do:

```
root # layman -a nightmare
```

* * *

## sys-apps/portage

Since version 2.2.16, Portage has a native mechanism for adding overlays.

Just copy `nightmare.conf` to your `/etc/portage/repos.conf` directory:

```
root # mkdir -p /etc/portage/repos.conf
root # wget https://gitlab.com/r3lgar/nightmare/raw/master/Documentation/repos.conf/nightmare.conf -O /etc/portage/repos.conf/nightmare.conf
```

Edit the "path" variable in `/etc/portage/repos.conf/nightmare.conf` to suit your overlay storage path 
(for example, `/usr/portage/nightmare` or `/mnt/portage/overlays/nightmare`).
Now you can sync the overlay using `emaint sync -r nightmare`.
